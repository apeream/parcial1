describe('Los estudiantes', function() {
    it('Login and Logout Los Estudiantes.co', function() {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        cy.contains('Ingresar').click({force: true})
        cy.get('.cajaLogIn').find('input[name="correo"]').click().type('pa2018@example.com')
        cy.get('.cajaLogIn').find('input[name="password"]').click().type('foobar1234')
        cy.get('.cajaLogIn').contains('Ingresar').click()
        cy.get('button[id="cuenta"]').click({force: true})
        cy.contains('Salir').click()
    })
    it('Search for teacher Mario Linares in Maestria Ingenieria de Software', function() {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        cy.get('.opcion_maestria_click').click()
        cy.get('.select').select('universidad-de-los-andes,maestria,maestrIa-en-ingenierIa-de-software')
        cy.contains('Mario Linares Vasquez')
    })
    
})